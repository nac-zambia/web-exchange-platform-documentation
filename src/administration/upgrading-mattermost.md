# Upgrading Mattermost

To upgrade to the latest version of Mattermost, please refer to the [official Mattermost documentation](https://docs.mattermost.com/upgrade/upgrading-mattermost-server.html).

While Matttermost has several releases during the year, we recommend upgrading between [Extended Support Release](https://docs.mattermost.com/upgrade/extended-support-release.html) versions. See the [Mattermost Version Archive](https://docs.mattermost.com/upgrade/version-archive.html) for the full list of versions available for download.
