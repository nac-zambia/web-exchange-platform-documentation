# Mobile

The following section gives information about how to use the Web Exchange Platform using the Mattermost mobile application.

Mattermost is available on:
- The [Apple App Store](https://apps.apple.com/us/app/mattermost/id1257222717)
- The [Google Play Store](https://play.google.com/store/apps/details?id=com.mattermost.rn&hl=en&gl=US)

Before using this section:
1. [Sign up](web/sign-up.md) using the web version of the Web Exchange Platform
1. Download the app on your device
