# Set Up

After downloading the Mattermost mobile app, open it. When you open the app for the first time, you will be asked to provide a server URL. Enter the following URL:

> <https://wep.nac.org.zm>

and then click "Connect".

![Mobile URL setup](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/mobile-set-up.jpg)
