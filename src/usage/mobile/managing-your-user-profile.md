# Managing Your User Profile

The first time you sign in to the platform, we need you to update your profile to make it obvious to your colleagues what your name is, which region you work in, what you look like and how you can be contacted.

To manage your profile on the mobile app, start by clicking the three vertical lines in the top right of the Web Exchange Platform app.

![Click the three vertical lines icon in the top right of the app](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/mobile-three-dots-menu.jpg)

From the options that pop-up, click, "Edit Profile".

![Click the hamburger menu](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/mobile-click-edit-profile.jpg)

In the "Edit Profile" screen update your information including uploading a clear profile picture of yourself. You may need to scroll to see the full range of fields you can adjust.

![Update your account settings](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/mobile-update-profile.png)
