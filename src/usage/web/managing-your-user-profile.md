# Managing Your User Profile

The first time you sign in to the platform, we need you to update your profile to make it obvious to your colleagues what your name is, which region you work in, what you look like and how you can be contacted.

To manage your profile, start by clicking your profile icon in the top-right.

![Click your profile icon](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-profile-icon.png)

From the options that pop-up, click, "Profile".

![Click profile](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-click-profile.png)

In the "Profile Settings" screen, update your information including uploading a clear profile picture of yourself.

![Update your profile settings](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-update-your-profile-settings.png)
