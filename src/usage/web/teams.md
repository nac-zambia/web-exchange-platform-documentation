# Teams

There are currently two "teams" setup on the Web Exchange Platform:

1. [Documents](https://wep.nac.org.zm/documents) (DO)
1. [NAC](https://wep.nac.org.zm/nac) (NA)

A team is like a universe or group. We currently use teams to group similar types of channels together.

![Teams](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-teams.png)

The [Documents](https://wep.nac.org.zm/documents) (DO) team has document channels where we can discuss best practices, share feedback and offer reflections on each document. Each channel addresses on document.

The [NAC](https://wep.nac.org.zm/nac) (NA) team has channels relating to day-to-day work for NAC, PACAs, DACAs and other relevant groups.
