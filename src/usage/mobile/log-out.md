# Log Out

To log out, click on the three vertical dots in the top-right of the Web Exchange Platform app.

![Click the three vertical lines icon in the top right of the app](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/mobile-three-dots-menu.jpg)

From the menu, click on "Logout" to log out.

![Log out](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/mobile-log-out.jpg)
