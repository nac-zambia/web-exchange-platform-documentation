# Sign In

To sign in to the Web Exchange Platform on your mobile device, open the app and enter your username or email and secure password and then click "Sign in".

![Sign in](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/mobile-sign-in.jpg)

If you don't see a sign in screen, please check that you have done the initial [set up](set-up.md) for the app on your device.
