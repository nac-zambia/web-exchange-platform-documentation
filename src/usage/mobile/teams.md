# Teams

There are currently two "teams" setup on the Web Exchange Platform:

1. [Documents](https://wep.nac.org.zm/documents)
1. [NAC](https://wep.nac.org.zm/nac)

A team is like a universe or group. We currently use teams to group similar types of channels together.

The [Documents](https://wep.nac.org.zm/documents) team has document channels where we can discuss best practices, share feedback and offer reflections on each document. Each channel addresses on document.

The [NAC](https://wep.nac.org.zm/nac) team has channels relating to day-to-day work for NAC, PACAs, DACAs and other relevant groups.
