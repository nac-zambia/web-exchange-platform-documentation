# Usage

The following sections show you how to use the Web Exchange platform on a day-to-day basis. This includes signing in, managing your user account and interacting with your colleagues.

The Web Exchange platform is available on:

- [Web](usage/web.md)
- [Mobile](usage/mobile.md)
