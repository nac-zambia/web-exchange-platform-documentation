# Managing Mattermost

In order to support the platform adequately, you will need to know what version of Mattermost you are managing. To determine the version of Mattermost you have installed:

Click on the hamburger menu (the three horizontal lines) next to your profile picture.

![View the platform version number](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/view-the-platform-version-number.png)

Click `About Web Exchange Platform` (just above `Log Out`).

![Click 'About Web Exchange Platform'](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/click-about-web-exchange-platform.png)

The version numbers you need will be displayed in the window that pops-up.

![Click on the hamburger menu](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/click-on-the-hamburger-menu.png)
