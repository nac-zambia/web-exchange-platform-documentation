# Messages

The Web Exchange Platform is meant to allow us to communicate. As such, messages are the most important feature of the platform.

To write a message, select the appropriate channel then write in the channel by doing the following:
- Navigate to the message box at the bottom of the Web Exchange Platform
- Type your message
- Press the ENTER key on your keyboard

![Message](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-message.png)

## Direct Messages

Messages in channels are visible to everyone in that channel. To send a direct message to an individual and only that individual you can send them a "direct message".

To send a direct message, click the "+" button next in the "Direct Messages" section.

![New direct message](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-new-direct-message.png)

Use the search to find the person you would like to direct message. Click on their name and then press "Go" to start to message them.

![New direct message search](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-new-direct-message-search.png)

Alternatively, you can click on click on the profile image of the individual you want to message and then click "Send Message".

![Direct message from profile](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-direct-message-from-profile.png)

## Uploads

To upload a file (e.g. video, document, picture or audio) with your message, click on the paperclip icon.

![Uploads](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-uploads.png)

This will allow you to select a file from your device to upload to the Web Exchange Platform. It will take a moment for the file to upload. To post the document, hit "ENTER" once the file uploads. We recommend adding a message with your file to give context so type your message and then hit "ENTER" to post the file.

## Emoji

To post a emoji with your message, click the emoji icon in the message box.

![Emoji](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-emoji.png)

Select the appropriate emoji by clicking on it (you may need to scroll up or down or use the search function to find the one you like).

![Emojis](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-emojis.png)

## Formatting Messages

The Web Exchange Platform uses [Markdown](https://docs.mattermost.com/channels/format-messages.html) to allow you to format messages. Formatting includes creating links, adding *bold* and _italics_, displaying lists and more.

## Editing Messages

Unlike with WhatsApp, you can edit your own messages on the Web Exchange Platform. This can help in situations where, for example, your message contains a spelling error.

![Click edit message](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-click-edit-message.png)

To edit a message you wrote:
- move your mouse over the message
- click on the three dots that pop-up over the message
- click "Edit"
- make your changes
- click the "Save" button

![Edit message](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-edit-message.png)

## Deleting Messages

To delete messages:
- move your mouse over the message
- click on the three dots that pop-up over the message
- click "Delete"

Deleting a message cannot be undone.

![Delete message](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-delete-message.png)

## Replying to a Message

As a channel grows, many messages accumulate, one after another. You can choose to reply to a single message. A reply helps to organise responses to a particular message. A reply branches a specific conversation while allowing the rest of the channel conversations to flow.

To reply to a message:
- move your mouse over the message
- click on the three dots that pop-up over the message
- click the reply icon (it looks like an arrow)
- type you message in the usual way (you can add files and emoji)
- click the "Save" button

If a message already has replies, you will see the reply icon on it with the number of replies next to it.

![Relpy to a message](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-reply.png)
