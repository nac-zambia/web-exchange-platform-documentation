# Log Out

To log out, your profile icon and then click "Log Out" at the bottom of the menu.

![Log out](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-log-out.png)
