# Administration

The Web Exchange Platform is powered by the self-hosted version of [Mattermost](https://mattermost.com/).

In this document, we'll share basic administration tasks. For more in-depth and specific documentation, please refer to the [Mattermost documentation](https://docs.Mattermost.com/).

As a platform administrator, we recommend that you join the [Matttermost Discussion Forums](https://forum.mattermost.com/). This will give you insight into the development of the platform as well as participate in the community.

## Configuration Summary

OS: Ubuntu 20.04 (LTS)
Database: MySQL
