# Introduction

Welcome to the Web Exchange Platform documentation.

The Web Exchange Platform is a project by the National HIV/AIDS/STI/TB Council of Zambia (NAC), the Deutsche Gesellschaft für Internationale Zusammenarbeit GmbH (GIZ), Syspons GmbH and BongoHive Consult Limited to help Provincial AIDS Coordination Advisors (PACAs) and District AIDS Coordination Advisors (DACAs) across Zambia to communicate more effectively with the NAC national office team and among each other.

The Web Exchange Platform is built on [Mattermost](https://mattermost.org/).

> Mattermost is an open-source, self-hostable online chat service with file sharing, search, and integrations. It is designed as an internal chat for organisations and companies, and mostly markets itself as an open-source alternative to Slack and Microsoft Teams.
>
> Ref: <https://en.wikipedia.org/wiki/mattermost>

![Welcome](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/welcome.png)

This document will give you tips and tricks on how to use the Web Exchange Platform day-to-day as a user or as an administrator.

The URL for this published documentation is:
> <https://nac-zambia.gitlab.io/web-exchange-platform-documentation/>

The URL for the Web Exchange Platform is:

> <https://wep.nac.org.zm>

If you have questions, feedback or contributions, please direct them to NAC:

- Andrew Chanda
- Gibson Mizinga
- Nyewani Soko
- Rita Kalamatila
- Jean Simalonda

## Thanks

- [NAC](https://www.nac.org.zm)
- [GIZ](https://www.giz.de/en/worldwide/338.html)
- [Syspons](https://www.syspons.com)
- [BongoHive Consult](https://bongohive.co.zm/consult)
