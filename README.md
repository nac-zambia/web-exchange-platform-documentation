# Web Exchange Platform Documentation

Documentation for the NAC Zambia Web Exchange Platform.

This documentation is created with [mdBook](https://rust-lang.github.io/mdBook/).

You can view the published version at: <https://nac-zambia.gitlab.io/web-exchange-platform-documentation>

## Thanks

- [NAC](https://www.nac.org.zm)
- [GIZ](https://www.giz.de/en/worldwide/338.html)
- [Syspons](https://www.syspons.com)
- [BongoHive Consult](https://bongohive.co.zm/consult)
