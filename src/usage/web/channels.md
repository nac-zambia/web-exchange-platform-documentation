# Channels

We have setup several channels in the NAC and Document teams for us to all collaborate with each other. A channel is like a WhatsApp group for a particular topic. Unlike WhatsApp groups, as a new member of the channel you have access to the entire conversation history going back to the moment the channel was created.

![Channels](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-channels.png)

If you are looking for a channel, use the "Find Channel" button. If the channel exists, click on the channel name and then click on "Join Channel" to see the conversations in that channel.

![Channels](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-find-channel.png)

To create a channel, click the "+" button next to the team name and then click "Create New Channel".

![Create channel](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-create-channel.png)

A channel can be either private (only accessible to people you invite) or public (accessible to everyone on the Web Exchange Platform). Make sure the channel has a Name, Purpose and Header then click the "Create Channel" button to create the channel.

![New channels](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/web-new-channel.png)
