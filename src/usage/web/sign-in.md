# Sign In

To sign in to the Web Exchange Platform, please visit: <http://wep.nac.org.zm>. From this page, please enter your username or email and secure password and then click "Sign in" to sign in.

![Sign in](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/sign-in.png)
