# Sign Up

Access to the Web Exchange Platform is by invitation from one of the platform admins. You will receive your platform invitation as an invite link.

## Sign Up via Link

To sign up via the platform invitation sign up link, click on it to begin the sign up process.

![Create an account on the Web Exchange Platform](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/create-an-account-on-the-web-exchange-platform.png)

Fill in your email, choose a username and choose a secure password. Make sure that you use a real email address and remember your username and password. Without this information, you will have problems signing in to the platform.

![Check your email inbox](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/check-your-inbox.png)

Once you complete this step, you will receive an email at the address you provided. To complete your the sign up process, please click the "Verify Email" link in the email you received from the platform.

![Verify your email address](https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/verify-email.png)

In case you don't receive the email in your inbox, please check your spam folder.

## Video Tutorial

<video width="320" height="240" controls>
  <source src="https://web-exchange-platform-documentation.s3.af-south-1.amazonaws.com/video-web-sign-up.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
